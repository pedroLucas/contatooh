var mongoose = require('mongoose');

module.exports = function(){	
	var schema = mongoose.Schema({
		nome: {
			type: String,
			required: true
		},
		email: {
			type: String,
			required: true,
			index: {
				unique: true
			}
		},
		emergencia: {
			type: mongoose.Schema.ObjectId,
			ref: 'Contato'
		}
	});
	
	schema.pre('save', function(next) {
		console.log("salvou e executou o pre");
		next();
	});
	
	schema.post('update', function(next) {
		console.log("salvou e executou o pos");
		next();
	});
	return mongoose.model('Contato', schema);
	
};