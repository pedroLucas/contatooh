module.exports = function(app){
	var controller = app.controllers.user;
	app.route('/users')
		.get(controller.save)
		.post(controller.save);
	app.route('/users/:id')
		.get(controller.save)
		.delete(controller.save);
}