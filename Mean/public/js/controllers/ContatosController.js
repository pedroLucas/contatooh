angular.module('contatooh').controller('ContatosController', 
	function($scope, Contato){
		$scope.mensagem = {texto: ""};
		$scope.filtro = '';//campo text do filtro na view
		$scope.contatos = [];
		$scope.init =  function(){
			buscaContatos();
		}
	
		function buscaContatos(){
			//Contatos.$promise.then(function(contatos){}).catch(function(erro){});
			Contato.query(
				function(contatos){
					$scope.mensagem = {};
					$scope.contatos = contatos;
				},
				function(erro){
					$scope.mensagem = {texto: "Não foi possivel obter lista."}
					console.log(erro);
				}
			);
		}
	
		$scope.remove = function(contato){
			Contato.delete({id: contato._id},
				buscaContatos,
				function(){
					console.log("Não foi possivel remover o contato.")
					console.log(erro);
				});
		};
		$scope.init();
	}
);