angular.module('contatooh').controller('ContatoController', 
	function($scope, $routeParams, Contato){
		
		$scope.salva = function(){
			$scope.contato.$save()
				.then(function(){
					$scope.mensagem = {texto: "Salvo com sucesso"};
					//limpa o form
					$scope.contato =  new Contato();
				})
				.catch(function(erro){
					$scope.mensagem = {texto: "Não foi possivel salvar."};
				});
		};
		
		Contato.query(function(contatos){
			$scope.contatos = contatos;
		});
	
		if($routeParams.contatoId){
			Contato.get({id: $routeParams.contatoId},
				function(contato){
					$scope.contato = contato;
				},
				function(erro){
					$scope.mensagem = {texto: "Não foi possível obter o contato."};
				}
			);	
		} else{
			/*Isso não dará erro em nosso formulário, que depende dos atributos do contato?
			 *  Não, porque o two-way data binding criará as propriedades dinamicamente no 
			 *  objeto caso elas não existam, através da diretiva ng-model*/
			$scope.contato = new Contato();
		}
	}
);